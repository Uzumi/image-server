const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const { sequelize, ORBModel, BRISKModel, AKAZEModel } = require('./models')
const config = require('./config/config')
const fs = require('fs')
const path = require('path')
const OrbHelper = require('./helpers/OrbHelper')
const BRISKHelper = require('./helpers/BRISKHelper')
const AKAZEHelper = require('./helpers/AKAZEHelper')

const app = express()
app.use(morgan('combined'))
app.use('/static', express.static('static'))
app.use(bodyParser.json())
app.use(cors())

require('./routes')(app)

sequelize.sync()
  .then(() => {
    app.listen(config.port, config.address)
    console.log(`Server started on ${config.address}:${config.port}`)
    fs.readdirSync(path.join(__dirname, '../static/'))
      .filter((dir) => fs.lstatSync(path.join(__dirname, '../static/', dir)).isDirectory() && dir !== '.DS_Store')
      .forEach((dir) => {
        console.log(dir)
        fs.readdirSync(path.join(__dirname, '../static/', dir))
          .filter((file) => fs.lstatSync(path.join(__dirname, '../static/', dir, file)).isFile() && file !== '.DS_Store')
          .forEach(file => {
            console.log(file)
            for (let i = 0; i < 3; i++) {
              switch (i) {
                case 0: {
                  OrbHelper.getOrbResult(fs.lstatSync(path.join(__dirname, '../static/', dir, file)), {}).then(resultOrb => {
                    ORBModel.findOne({
                      where: {
                        imageName: path.join(__dirname, '../static/', dir, file)
                      }
                    }).then(orbModel => {
                      if (!orbModel) {
                        ORBModel.create({
                          imageName: path.join(__dirname, '../static/', dir, file),
                          hash: resultOrb
                        })
                      }
                    })
                  })
                  break
                }
                case 1: {
                  BRISKHelper.getBRISKResult(fs.lstatSync(path.join(__dirname, '../static/', dir, file)), {}).then(resultBRISK => {
                    BRISKModel.findOne({
                      where: {
                        imageName: path.join(__dirname, '../static/', dir, file)
                      }
                    }).then(briskModel => {
                      if (!briskModel) {
                        BRISKModel.create({
                          imageName: path.join(__dirname, '../static/', dir, file),
                          hash: resultBRISK
                        })
                      }
                    })
                  })
                  break
                }
                case 2: {
                  AKAZEHelper.getAKAZEResult(fs.lstatSync(path.join(__dirname, '../static/', dir, file)), {}).then(resultAKAZE => {
                    AKAZEModel.findOne({
                      where: {
                        imageName: path.join(__dirname, '../static/', dir, file)
                      }
                    }).then(akazeModel => {
                      if (!akazeModel) {
                        AKAZEModel.create({
                          imageName: path.join(__dirname, '../static/', dir, file),
                          hash: resultAKAZE
                        })
                      }
                    })
                  })
                  break
                }
                default:
                  break
              }
            }
          })
      })
  })
