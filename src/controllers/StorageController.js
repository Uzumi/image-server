const fs = require('fs')
const path = require('path')
const config = require('../config/config')
const SaveFileUtil = require('./../utils/SaveFileUtil')
const OrbHelper = require('./../helpers/OrbHelper')
const BRISKHelper = require('./../helpers/BRISKHelper')
const AKAZEHelper = require('./../helpers/AKAZEHelper')

const errorInternal = 'Произошла внутренняя ошибка на сервере. Попробуйте еще раз!'

const COUNT_FILES = 8

module.exports = {
  async getMethods (req, res) {
    try {
      let methods = ['0', '2']
      res.status(200).send({
        baseUrl: 'http://' + config.address + ':' + config.port + '/static/',
        methods: methods
      })
    } catch (err) {
      return res.status(500).send({
        error: errorInternal
      })
    }
  },
  async getDirs (req, res) {
    try {
      let dirs = []
      let index = 0
      fs.readdirSync(path.join(__dirname, '../../static'))
        .filter((file) => fs.lstatSync(path.join(__dirname, '../../static', file)).isDirectory())
        .forEach((file) => {
          dirs[index] = file
          index++
        })
      res.status(200).send({
        baseUrl: 'http://' + config.address + ':' + config.port + '/static/',
        directories: dirs
      })
    } catch (err) {
      return res.status(500).send({
        error: errorInternal
      })
    }
  },
  async getFiles (req, res) {
    try {
      let directory = req.params.directory
      let since = req.params.since
      let files = []
      let index = 0
      let k = 0
      fs.readdirSync(path.join(__dirname, '../../static/', directory))
        .filter((file) => fs.lstatSync(path.join(__dirname, '../../static/', directory, file)).isFile())
        .forEach((file) => {
          if (k >= since && index + 1 <= COUNT_FILES) {
            files[index] = file
            index++
          }
          k++
        })
      res.status(200).send({
        baseUrl: 'http://' + config.address + ':' + config.port + '/static/',
        directory: directory,
        files: files
      })
    } catch (err) {
      return res.status(500).send({
        error: errorInternal
      })
    }
  },
  async addFile (req, res) {
    try {
      SaveFileUtil.addFile(req.params.directory)
      res.status(200).send({
        baseUrl: 'http://' + config.address + ':' + config.port + '/static/'
      })
    } catch (err) {
      return res.status(500).send({
        error: errorInternal
      })
    }
  },
  async uploadFile (req, res) {
    try {
      let description = req.body.description
      let method = JSON.parse(description)
      let dirStatic = path.join(__dirname, '/../../static/')
      const imageUri = dirStatic + req.body.imageName
      let detectedFolder
      switch (method.number) {
        case 0:
          detectedFolder = await OrbHelper.checkImage(imageUri, method.parameters)
          console.log(detectedFolder)
          break
        case 1:
          detectedFolder = await BRISKHelper.checkImage(imageUri, method.parameters)
          console.log(detectedFolder)
          break
        case 2:
          detectedFolder = await AKAZEHelper.checkImage(imageUri, method.parameters)
          console.log(detectedFolder)
          break
        default:
          break
      }
      res.status(200).send({
        baseUrl: 'http://' + config.address + ':' + config.port + '/static/',
        imageName: req.body.imageName,
        category: detectedFolder
      })
    } catch (err) {
      return res.status(500).send({
        error: errorInternal
      })
    }
  }
}
