const multer = require('multer')
const StorageController = require('./controllers/StorageController')
const SaveFileHelper = require('./SaveFileHelper')

module.exports = (app) => {
  var uploadFile = multer({ storage: SaveFileHelper.storageFileUpload })
  app.get('/get_methods', StorageController.getMethods)
  app.get('/get_static_dirs', StorageController.getDirs)
  app.get('/get_files/:directory/:since', StorageController.getFiles)
  app.get('/add_file/:directory', StorageController.addFile)
  app.post('/upload_file', uploadFile.any(), StorageController.uploadFile)
}
