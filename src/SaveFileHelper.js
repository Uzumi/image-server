const multer = require('multer')
const path = require('path')
const SaveFileUtil = require('./utils/SaveFileUtil')

let storageFileUpload = multer.diskStorage({
  destination: function (req, file, cb) {
    SaveFileUtil.createDirectoryIfNotExists()
    cb(null, path.join(__dirname, `/../static/`))
  },
  filename: function (req, file, cb) {
    var tempFileName = 'image_' + Date.now()
    req.body.imageName = tempFileName
    cb(null, tempFileName)
  }
})

module.exports = {
  storageFileUpload: storageFileUpload
}
