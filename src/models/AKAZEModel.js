module.exports = (sequelize, DataTypes) => {
  const AKAZEModel = sequelize.define('AKAZEModel', {
    imageName: {
      type: DataTypes.STRING,
      unique: true
    },
    hash: {
      type: DataTypes.STRING
    }
  }
  )

  return AKAZEModel
}
