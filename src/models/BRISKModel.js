module.exports = (sequelize, DataTypes) => {
  const BRISKModel = sequelize.define('BRISKModel', {
    imageName: {
      type: DataTypes.STRING,
      unique: true
    },
    hash: {
      type: DataTypes.STRING
    }
  }
  )

  return BRISKModel
}
