module.exports = (sequelize, DataTypes) => {
  const ORBModel = sequelize.define('ORBModel', {
    imageName: {
      type: DataTypes.STRING,
      unique: true
    },
    hash: {
      type: DataTypes.STRING
    }
  }
  )

  return ORBModel
}
