module.exports = {
  port: process.env.PORT || 8081,
  address: process.env.ADDRESS || 'localhost',
  db: {
    database: process.env.DB_NAME || 'mydb',
    user: process.env.DB_USER || 'Uzumi',
    password: process.env.DB_PASS || '12345',
    options: {
      dialect: process.env.DIALECT || 'sqlite',
      host: process.env.HOST || 'localhost',
      storage: './mydb.sqlite'
    }
  }
}
