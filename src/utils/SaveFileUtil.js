const fs = require('fs')
const path = require('path')

module.exports = {
  createDirectoryIfNotExists () {
    let dirStatic = path.join(__dirname, '/../../static')
    if (!fs.existsSync(dirStatic)) {
      fs.mkdirSync(dirStatic)
    }
  },
  createOrClearDirectory () {
    let dirStatic = path.join(__dirname, '/../../static')
    if (!fs.existsSync(dirStatic)) {
      fs.mkdirSync(dirStatic)
    }
    fs.readdirSync(dirStatic).forEach(file => {
      if (file.indexOf('image', 0) !== -1) {
        fs.unlinkSync(dirStatic + '/' + file)
      }
    })
  },
  addFile (directory) {
    var dirStatic = path.join(__dirname, '/../../static')
    if (!fs.existsSync(dirStatic)) {
      fs.mkdirSync(dirStatic)
    }
    fs.readdirSync(dirStatic).filter(file => !fs.lstatSync(path.join(dirStatic, `/${file}`)).isDirectory()).forEach(file => {
      if (file.indexOf('image', 0) !== -1) {
        let dirCategory = dirStatic + `/` + directory
        if (!fs.existsSync(dirCategory)) {
          fs.mkdirSync(dirCategory)
        }
        let newFilename = `/` + directory + `_` + Date.now()
        fs.createReadStream(dirStatic + `/image`).pipe(fs.createWriteStream(dirCategory + newFilename))
        fs.unlinkSync(dirStatic + '/' + file)
      }
    })
  }
}
